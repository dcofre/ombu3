﻿using ombu3.Kernel.Base.DTOs;

namespace ombu3.Kernel.Sec.DTO
{
    public class RegistrationDTO : BaseValidatableDTO
    {
        public string User { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
