﻿namespace ombu3.Kernel.Sec.DTO
{
    public class JwtDTO
    {
        public string id { get; set; }
        public string auth_token { get; set; }
        public int expires_in { get; set; }
        public string usr_home { get; set; }
    }
}
