﻿namespace ombu3.Kernel.Sec.DTO
{
    using ombu3.Kernel.Base.DTOs;

    public class UserDTO : IDTO
    {
        public string User { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
    }
}
