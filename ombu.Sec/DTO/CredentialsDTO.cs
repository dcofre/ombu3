﻿using ombu3.Kernel.Base.DTOs;

namespace ombu3.Kernel.Sec.DTO
{
    public class CredentialsDTO : IDTO
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
