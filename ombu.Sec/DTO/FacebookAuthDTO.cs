using ombu3.Kernel.Base.DTOs;

namespace ombu3.Kernel.Sec.DTO
{
    public class FacebookAuthDTO : IDTO
    {
        public string AccessToken { get; set; }
    }
}
