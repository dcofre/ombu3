﻿namespace ombu3.Kernel.Sec.DTO
{
    using ombu3.Kernel.Base.DTOs;

    public class RoleDTO : IDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
