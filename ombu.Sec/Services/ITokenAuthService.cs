﻿using ombu3.Kernel.Sec.DTO;
using Microsoft.AspNetCore.Authentication;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ombu3.Kernel.Sec.Services
{
    public interface ITokenAuthService 
    {
        ClaimsIdentity GenerateClaimsIdentity(string userId);
        JwtDTO GenerateJwtDTO(string userId);
        AuthenticateResult Authenticate(string token);
    }
}