﻿using System.Threading;
using System.Threading.Tasks;
using ombu3.Kernel.Sec.DTO;

namespace ombu3.Kernel.Sec.Web.Controllers
{
    public interface IFacebookService
    {
        Task<JwtDTO> Facebook(FacebookAuthDTO dto, CancellationToken cancellationToken);
    }
}