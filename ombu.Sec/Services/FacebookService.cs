namespace ombu3.Kernel.Sec.Web.Controllers
{
    using ombu3.Kernel.DAL;
    using ombu3.Kernel.Sec.Authorization;
    using ombu3.Kernel.Sec.DTO;
    using ombu3.Kernel.Sec.Services;
    using Microsoft.Extensions.Options;
    using Newtonsoft.Json;
    using System.ComponentModel.DataAnnotations;
    using System.Net.Http;
    using System.Threading;
    using System.Threading.Tasks;

    public class FacebookService : BaseDBService, IFacebookService
    {
        private readonly IUserService _userManager;
        private readonly FacebookAuthSettings _fbAuthSettings;
        private readonly ITokenAuthService _tokenAuthSvc;
        private static readonly HttpClient Client = new HttpClient();

        public FacebookService(IDBServiceDeps dbdeps, IUserService usrSvc, ITokenAuthService tokenAuthSvc, IOptions<FacebookAuthSettings> fbAuthSettingsAccessor) : base(dbdeps)
        {
            _fbAuthSettings = fbAuthSettingsAccessor.Value;
            _userManager = usrSvc;
            _tokenAuthSvc = tokenAuthSvc;
        }

        public async Task<JwtDTO> Facebook(FacebookAuthDTO dto, CancellationToken cancellationToken)
        {
            // 1.generate an app access token
            var appAccessTokenResponse = await Client.GetStringAsync($"https://graph.facebook.com/oauth/access_token?client_id={_fbAuthSettings.AppId}&client_secret={_fbAuthSettings.AppSecret}&grant_type=client_credentials");
            var appAccessToken = JsonConvert.DeserializeObject<FacebookAppAccessToken>(appAccessTokenResponse);
            // 2. validate the user access token
            var userAccessTokenValidationResponse = await Client.GetStringAsync($"https://graph.facebook.com/debug_token?input_token={dto.AccessToken}&access_token={appAccessToken.AccessToken}");
            var userAccessTokenValidation = JsonConvert.DeserializeObject<FacebookUserAccessTokenValidation>(userAccessTokenValidationResponse);

            if (!userAccessTokenValidation.Data.IsValid)
            {
                throw new ValidationException("Invalid facebook token.");
            }

            // 3. we've got a valid token so we can request user data from fb
            var userInfoResponse = await Client.GetStringAsync($"https://graph.facebook.com/v2.8/me?fields=id,email,first_name,last_name,name,gender,locale,birthday,picture&access_token={dto.AccessToken}");
            var userInfo = JsonConvert.DeserializeObject<FacebookUserData>(userInfoResponse);

            // 4. ready to create the local user account (if necessary) and jwt
            var user =  _userManager.FindByEmail(userInfo.Email);

            if (user == null)
            {
                var appUser = new AppUser
                {
                    name = userInfo.FirstName,
                    surname = userInfo.LastName,
                    //FacebookId = userInfo.Id,
                    email = userInfo.Email,
                    id = userInfo.Email
                    // PictureUrl = userInfo.Picture.Data.Url
                };

                var result =  _userManager.Create(appUser);

                if (result == null) throw new ValidationException("Couldnt create user");
            }

            // generate the jwt for the local user...
            var localUser =  _userManager.FindByEmail(userInfo.Email);

            if (localUser == null)
            {
                throw new ValidationException("Failed to create local user account.");
            }

            var jwt = this._tokenAuthSvc.GenerateJwtDTO(localUser.id);

            return jwt;
        }
    }
}
