﻿using ombu3.Kernel.Web;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System.Text.Encodings.Web;
using System.Threading.Tasks;

namespace ombu3.Kernel.Sec.Services
{
    public class TokenAuthHandler : AuthenticationHandler<TokenAuthServiceOptions>
    {
        private readonly TokenAuthServiceOptions _opts;

        protected ITokenAuthService Service { get; set; }

        public TokenAuthHandler(IOptionsMonitor<TokenAuthServiceOptions> options, ILoggerFactory logger, UrlEncoder encoder, ISystemClock clock, ITokenAuthService service)
            : base(options, logger, encoder, clock)
        {
            Service = service;
            this._opts = options.CurrentValue;
        }

        protected override Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            var token = this.Context.GetHeaderOrCookieValue("Authorization");
            return Task.FromResult(this.Service.Authenticate(token));
        }
    }
}
