﻿namespace ombu3.Kernel.Sec.Services
{
    using ombu3.Kernel.Base.Base.Exceptions;
    using ombu3.Kernel.DAL;
    using ombu3.Kernel.Sec.Authorization;
    using ombu3.Kernel.Sec.DTO;
    using Microsoft.AspNetCore.Http;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security;
    using System.Security.Claims;
    using System.Threading;

    public class UserService : CRUDService<UserDTO, AppUser, string>, IUserService
    {
        public UserService(IDBServiceDeps deps) : base(deps)
        {
        }

        public IList<Claim> GetClaims(AppUser user, CancellationToken cancellationToken)
        {
            var retVal = new List<Claim>();
            retVal.Add(new Claim("user_id", user.id));
            retVal.Add(new Claim("user_email", user.email));
            retVal.Add(new Claim("user_name", user.name));
            retVal.Add(new Claim("user_surname", user.surname));
            return retVal;
        }

        public AppUser FindByEmail(string normalizedEmail)
        {
            AppUser retVal = this.Repo.GetOne<AppUser>("select * from sec_user where email = @email", new { email = normalizedEmail });
            return retVal;
        }

        public AppUser Create(AppUser user)
        {
            user.id = user.id.TrimEnd().ToLower();
            var exists = this.Read(user.id.TrimEnd()) != null;

            if (exists)
            {
                throw new ValidationException(string.Format("User {0} already exists", user.id));
            }

            return this.Repo.Insert<AppUser>(user);
        }

        public UserDTO GetAuthenticatedUser(HttpContext ctx)
        {
            if (ctx.User == null) return null;

            var userId = ctx.User.Claims.Single(c => c.Type == "id");

            if (userId == null) throw new SecurityException("claim id wasnt found");

            return this.MapSvc.ToDTO<UserDTO, AppUser>(this.Repo.GetOne<AppUser>(userId.Value));
        }

        public string UsrHomeUrl
        {
            get
            {
                return "api/userdashboard/";
            }
        }
    }
}
