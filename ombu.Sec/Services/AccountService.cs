﻿namespace ombu3.Kernel.Sec.Web.Controllers
{
    using ombu3.Kernel.Base.Base.Exceptions;
    using ombu3.Kernel.Base.Base.Validation;
    using ombu3.Kernel.DAL;
    using ombu3.Kernel.Sec.Authorization;
    using ombu3.Kernel.Sec.DTO;
    using ombu3.Kernel.Sec.Services;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;

    public class AccountService : BaseDBService, IAccountService
    {
        private readonly IUserService _usrSvc;

        public AccountService(IDBServiceDeps deps, IUserService usrSvc) : base(deps)
        {
            _usrSvc = usrSvc;
        }

        public async Task<RegistrationDTO> CreateAccount(RegistrationDTO dto, CancellationToken cancellationToken)
        {
            var retVal = new RegistrationDTO();
            var verrs = new List<ValidationError>();

            if (_usrSvc.Read(dto.User.TrimEnd()) != null)
            {
                verrs.Add(new ValidationError("User", string.Format("User {0} already exists", dto.User.TrimEnd())));
            }

            if (_usrSvc.FindByEmail(dto.Email.TrimEnd()) != null)
            {
                verrs.Add(new ValidationError("Email", string.Format("Email {0} is already registered", dto.Email.TrimEnd())));
            }

            var userIdentity = new AppUser()
            {
                id = dto.User.TrimEnd(),
                email = dto.Email.TrimEnd(),
                name = dto.FirstName.TrimEnd(),
                surname = dto.LastName.TrimEnd(),
                password = dto.Password.TrimEnd(),
            };

            if (verrs.Any() || dto.Validate)
            {
                userIdentity.Validate();
                verrs.AddRange(userIdentity.ValidationErrors);
                this.DoValidation(verrs);
            }
            else
            {
                userIdentity = this.Repo.Insert<AppUser>(userIdentity);
                retVal.User = userIdentity.id;
                retVal.Email = userIdentity.email;
                retVal.Password = "****";
                retVal.FirstName = userIdentity.name;
                retVal.LastName = userIdentity.surname;
            }

            return retVal;
        }

    }
}
