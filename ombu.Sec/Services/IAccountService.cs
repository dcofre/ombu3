﻿using System.Threading;
using System.Threading.Tasks;
using ombu3.Kernel.Sec.DTO;

namespace ombu3.Kernel.Sec.Web.Controllers
{
    public interface IAccountService
    {
        Task<RegistrationDTO> CreateAccount(RegistrationDTO model, CancellationToken cancellationToken);
    }
}