﻿using System.Threading;
using System.Threading.Tasks;
using ombu3.Kernel.DAL;
using ombu3.Kernel.Sec.Authorization;
using ombu3.Kernel.Sec.DTO;
using Microsoft.AspNetCore.Identity;

namespace ombu3.Kernel.Sec.Services
{
    public interface IRoleService : ICRUDService<RoleDTO, string>, IRoleStore<Role>
    {
    }
}