﻿using ombu3.Kernel.Sec.DTO;

namespace ombu3.Kernel.Sec.Web.Controllers
{
    public interface ILoginService
    {
        JwtDTO Login(CredentialsDTO credentials);
    }
}