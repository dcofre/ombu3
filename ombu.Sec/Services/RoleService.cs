﻿namespace ombu3.Kernel.Sec.Services
{
    using ombu3.Kernel.DAL;
    using ombu3.Kernel.Sec.Authorization;
    using ombu3.Kernel.Sec.DTO;
    using Microsoft.AspNetCore.Identity;
    using System;
    using System.Threading;
    using System.Threading.Tasks;

    /// <summary>
    /// Class that implements the key ASP.NET Identity role store iterfaces
    /// </summary>
    public class RoleService : CRUDService<RoleDTO, Role, string>, IRoleService
    {
        public RoleService(IDBServiceDeps deps) : base(deps)
        {
        }

        public async Task<IdentityResult> CreateAsync(Role role, CancellationToken cancellationToken)
        {
            IdentityResult retVal = new IdentityResult();
            if (!cancellationToken.IsCancellationRequested)
            {
                role = this.Repo.Insert<Role>(role);
                if (role != null) retVal = IdentityResult.Success;
            }
            return retVal;
        }

        public async Task<IdentityResult> DeleteAsync(Role role, CancellationToken cancellationToken)
        {
            IdentityResult retVal = new IdentityResult();
            if (!cancellationToken.IsCancellationRequested)
            {
                var ok = this.Repo.Delete<Role>(role);
                if (ok) retVal = IdentityResult.Success;
            }
            return retVal;
        }

        public void Dispose()
        {

        }

        public async Task<Role> FindByIdAsync(string roleId, CancellationToken cancellationToken)
        {
            Role retVal = null;
            if (!cancellationToken.IsCancellationRequested)
            {
                retVal = this.Repo.GetOne<Role>(roleId.Trim().ToUpper());
            }
            return retVal;
        }

        public async Task<Role> FindByNameAsync(string normalizedRoleName, CancellationToken cancellationToken)
        {
            return await this.FindByIdAsync(normalizedRoleName, cancellationToken);
        }

        public async Task<string> GetNormalizedRoleNameAsync(Role role, CancellationToken cancellationToken)
        {
            return await this.GetRoleIdAsync(role, cancellationToken);
        }

        public async Task<string> GetRoleIdAsync(Role role, CancellationToken cancellationToken)
        {
            return this.GetX<string>(role, cancellationToken, r => r.id);
        }

        public async Task<string> GetRoleNameAsync(Role role, CancellationToken cancellationToken)
        {
            return await this.GetRoleIdAsync(role, cancellationToken);
        }

        public async Task SetNormalizedRoleNameAsync(Role role, string normalizedName, CancellationToken cancellationToken)
        {
           await this.SetRoleNameAsync(role, normalizedName, cancellationToken);
        }

        public async Task SetRoleNameAsync(Role role, string roleName, CancellationToken cancellationToken)
        {
            this.SetX(role, cancellationToken, r => r.id = roleName.Trim().ToUpper());
        }

        public async Task<IdentityResult> UpdateAsync(Role role, CancellationToken cancellationToken)
        {
            IdentityResult retVal = new IdentityResult();
            if (!cancellationToken.IsCancellationRequested)
            {
                role = this.Repo.Update<Role>(role);
                if (role != null) retVal = IdentityResult.Success;
            }
            return retVal;
        }

        private void SetX(Role role, CancellationToken cancellationToken, Action<Role> predicate)
        {
            if (!cancellationToken.IsCancellationRequested)
            {
                predicate.Invoke(role);
            }
        }

        private TRet GetX<TRet>(Role role, CancellationToken cancellationToken, Func<Role, TRet> predicate)
        {
            TRet retVal = default(TRet);
            if (!cancellationToken.IsCancellationRequested)
            {
                retVal = predicate.Invoke(role);
            }
            return retVal;
        }
    }
}
