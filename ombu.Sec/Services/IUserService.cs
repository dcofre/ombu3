﻿using System.Collections.Generic;
using System.Security.Claims;
using System.Threading;
using ombu3.Kernel.DAL;
using ombu3.Kernel.Sec.Authorization;
using ombu3.Kernel.Sec.DTO;
using Microsoft.AspNetCore.Http;

namespace ombu3.Kernel.Sec.Services
{
    public interface IUserService: ICRUDService<UserDTO, string>
    {
        AppUser Create(AppUser user);
        AppUser FindByEmail(string normalizedEmail);
        UserDTO GetAuthenticatedUser(HttpContext ctx);
        IList<Claim> GetClaims(AppUser user, CancellationToken cancellationToken);
        string UsrHomeUrl { get; }
    }
}