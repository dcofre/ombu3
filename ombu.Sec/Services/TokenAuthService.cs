﻿using ombu3.Kernel.DAL;
using ombu3.Kernel.Sec.DTO;
using ombu3.Kernel.Web;
using Microsoft.AspNetCore.Authentication;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;

namespace ombu3.Kernel.Sec.Services
{
    public class TokenAuthService : ConfigurableService, ITokenAuthService
    {
        public const string SchemaName = "bt.Kernel.Sec.TokenAuth";
        public const string TokenType = "Bearer";

        protected TokenAuthServiceOptions Options { get; set; }
        protected IUserService Service { get; set; }

        public TokenAuthService(IConfigurableServiceDeps deps, IUserService service, TokenAuthServiceOptions opts) : base(deps)
        {
            this.Service = service;
            this.Options = opts;
        }

        public ClaimsIdentity GenerateClaimsIdentity(string userId)
        {
            return new ClaimsIdentity(new GenericIdentity(userId), new[]
            {
                new Claim(Helpers.Constants.Strings.JwtClaimIdentifiers.Id, userId),
                new Claim(Helpers.Constants.Strings.JwtClaimIdentifiers.Rol, Helpers.Constants.Strings.JwtClaims.ApiAccess)
            });
        }

        public JwtDTO GenerateJwtDTO(string userId)
        {
            return this.GenerateJwtDTO(this.GenerateClaimsIdentity(userId), userId);
        }

        public AuthenticateResult Authenticate(string token)
        {

            if (string.IsNullOrEmpty(token))
            {
                return AuthenticateResult.Fail("There's no Authorization header");
            }
            else if (!token.StartsWith(TokenType + " "))
            {
                return AuthenticateResult.Fail(string.Format("There's no {0} token", TokenType));
            }

            var claims = this.ToClaims(token.Substring(TokenType.Length + 1));

            if (claims != null)
            {
                var ticket = new AuthenticationTicket(claims, TokenAuthService.SchemaName);
                return AuthenticateResult.Success(ticket);
            }
            else
            {
                return AuthenticateResult.Fail("Token not authorized");
            }
        }

        private string GenerateEncodedToken(string userName, ClaimsIdentity identity)
        {
            var claims = new[]
         {
                 new Claim(JwtRegisteredClaimNames.Sub, userName),
                 new Claim(JwtRegisteredClaimNames.Jti,  this.Options.JtiGenerator()),
                 new Claim(JwtRegisteredClaimNames.Iat, ToUnixEpochDate(this.Options.IssuedAt).ToString(), ClaimValueTypes.Integer64),
                 identity.FindFirst(Helpers.Constants.Strings.JwtClaimIdentifiers.Rol),
                 identity.FindFirst(Helpers.Constants.Strings.JwtClaimIdentifiers.Id)
             };

            // Create the JWT security token and encode it.
            var jwt = new JwtSecurityToken(
                issuer: this.Options.Issuer,
                audience: this.Options.Audience,
                claims: claims,
                notBefore: this.Options.NotBefore,
                expires: this.Options.Expiration,
                signingCredentials: this.Options.SigningCredentials);

            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

            return encodedJwt;
        }

        /// <returns>Date converted to seconds since Unix epoch (Jan 1, 1970, midnight UTC).</returns>
        private long ToUnixEpochDate(DateTime date)
          => (long)Math.Round((date.ToUniversalTime() -
                               new DateTimeOffset(1970, 1, 1, 0, 0, 0, TimeSpan.Zero))
                              .TotalSeconds);

        private void ThrowIfInvalidOptions(TokenAuthServiceOptions options)
        {
            if (options == null) throw new ArgumentNullException(nameof(options));

            if (options.ValidFor <= TimeSpan.Zero)
            {
                throw new ArgumentException("Must be a non-zero TimeSpan.", nameof(TokenAuthServiceOptions.ValidFor));
            }

            if (options.SigningCredentials == null)
            {
                throw new ArgumentNullException(nameof(TokenAuthServiceOptions.SigningCredentials));
            }

            if (options.JtiGenerator == null)
            {
                throw new ArgumentNullException(nameof(TokenAuthServiceOptions.JtiGenerator));
            }
        }

        private JwtDTO GenerateJwtDTO(ClaimsIdentity identity, string userName)
        {
            var response = new JwtDTO()
            {
                id = identity.Claims.Single(c => c.Type == "id").Value,
                auth_token = this.GenerateEncodedToken(userName, identity),
                expires_in = (int)this.Options.ValidFor.TotalSeconds,
                usr_home = this.Service.UsrHomeUrl
            };

            return response;
        }

        private ClaimsPrincipal ToClaims(string token)
        {
            ClaimsPrincipal retVal = null;
            SecurityToken tok = null;
            var hndl = new JwtSecurityTokenHandler();
            var pars = new TokenValidationParameters()
            {
                ValidateIssuer = true,
                ValidIssuer = this.Options.Issuer,
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = this.Options.SigningCredentials.Key,
                ValidateAudience = true,
                ValidAudience = this.Options.Audience,
                ValidateLifetime = true
            };

            try
            {
                retVal = hndl.ValidateToken(token, pars, out tok);
            }
            catch
            { }

            return retVal;
        }
    }
}
