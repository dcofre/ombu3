﻿namespace ombu3.Kernel.Sec.Web.Controllers
{
    using ombu3.Kernel.Base.Base.Exceptions;
    using ombu3.Kernel.DAL;
    using ombu3.Kernel.Sec.Authorization;
    using ombu3.Kernel.Sec.DTO;
    using ombu3.Kernel.Sec.Services;
    using System.Security;
    using System.Threading.Tasks;

    public class LoginService : BaseDBService, ILoginService
    {
        private readonly IUserService _usrSvc;
        private readonly ITokenAuthService _authSvc;

        public LoginService(IDBServiceDeps deps, IUserService usrSvc, ITokenAuthService authSvc) : base(deps)
        {
            _usrSvc = usrSvc;
            _authSvc = authSvc;
        }

        public  JwtDTO Login(CredentialsDTO credentials)
        {
            JwtDTO jwt = null;
            var identity = this.Repo.GetOne<AppUser>(credentials.UserName);
            if (identity == null || identity.password != credentials.Password)
            {
                throw new ValidationException("Invalid username or password");
            }
            else
            {
                jwt =  this._authSvc.GenerateJwtDTO(credentials.UserName);
            }

            return jwt;
        }

    }
}
