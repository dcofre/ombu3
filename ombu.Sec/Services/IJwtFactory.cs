﻿namespace bt.Kernel.Sec.Services
{
    using System.Security.Claims;
    using System.Threading.Tasks;

    public interface IJwtFactory
    {
        Task<string> GenerateEncodedToken(string userId, ClaimsIdentity identity);
        ClaimsIdentity GenerateClaimsIdentity(string UserId);
    }
}
