﻿using ombu3.Kernel.Base.Base.Exceptions;
using ombu3.Kernel.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ombu3.Kernel.Sec.Authorization
{
    public class AppUser : BaseGenericEntity<string>
    {
        public AppUser()
        {
            this.Roles = new HashSet<string>();
        }

        public string name { get; set; }
        public string surname { get; set; }
        public string password { get; set; }
        public string email { get; set; }
        public string address { get; set; }
        public ISet<string> Roles { get; protected set; }
        public string stamp { get; set; }
        public string phonenumber { get; set; }
        public bool phonenumberconfirmed { get; set; }
        public bool emailconfirmed { get; set; }

        public void AddRole(string roleName)
        {
            roleName = roleName.Trim().ToLower();
            if (!this.Roles.Any(r => r.Trim().ToLower() == roleName))
            {
                this.Roles.Add(roleName);
            }
        }

        public void RemoveRole(string roleName)
        {
            this.Roles.Remove(roleName.Trim().ToLower());
        }

        public override void Validate()
        {
            if (this.id.Length > 16) this.AddError( "User id is too long. Max lenght is 16.");
            if (this.email.Length > 64) this.AddError("Email is too long. Max lenght is 64.");
            if (this.name.Length > 64) this.AddError("Name is too long. Max lenght is 64.");
            if (this.surname.Length > 64) this.AddError("Surname is too long. Max lenght is 64.");
        }
    }
}
