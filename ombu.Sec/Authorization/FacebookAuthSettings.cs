namespace ombu3.Kernel.Sec.Authorization
{
    public class FacebookAuthSettings
    {
      public string AppId { get; set; }
      public string AppSecret { get; set; }
    }
}
