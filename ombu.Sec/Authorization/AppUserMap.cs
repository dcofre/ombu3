﻿using DapperExtensions.Mapper;
using ombu3.Kernel.DAL.Entities;

namespace ombu3.Kernel.Sec.Authorization
{
    internal class AppUserMap: BaseEntityMap<AppUser>
    {
        public AppUserMap()
        {
            Table("sec_user");
            Map(x => x.id).Key(KeyType.Assigned);
            Map(x => x.name);
            Map(x => x.surname);
            Map(x => x.password);
            Map(x => x.email);
            //Map(x => x.address);
        }
    }
}
