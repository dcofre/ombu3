﻿using AutoMapper;
using ombu3.Kernel.Sec.Authorization;
using ombu3.Kernel.Sec.DTO;

namespace ombu3.Kernel.Sec.AppStart
{
    public class Startup
    {
        public static void Configure()
        {
            // DapperExtensions.DapperExtensions.SqlDialect = new PostgreSqlDialect();
            //DapperExtensions.DapperAsyncExtensions.SqlDialect = new PostgreSqlDialect();
            DapperExtensions.DapperExtensions.SetMappingAssemblies(new[] { typeof(AppUserMap).Assembly });
        }

        public static void CreateMaps(IMapperConfigurationExpression cfg)
        {
            cfg.CreateMap<UserDTO, AppUser>()
                    .ForMember(e => e.id, op => op.MapFrom(d => d.User))
                    .ReverseMap()
                    .ForMember(d => d.User, op => op.MapFrom(e => e.id));
        }
    }
}
