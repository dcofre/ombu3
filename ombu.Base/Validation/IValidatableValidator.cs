﻿namespace ombu3.Kernel.Base.Validation
{
    public interface IValidatableValidator
    {
        void Validate(IValidatable value);
    }
}
