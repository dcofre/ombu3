﻿namespace ombu3.Kernel.Base.Validation
{
    public class CUITValidator : RegexValidatorBase
    {
        public override string Regex
        {
            get
            {
                return @"^\d{9,11}$";
            }
        }
    }
}
