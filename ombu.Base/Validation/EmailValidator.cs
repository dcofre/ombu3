﻿namespace ombu3.Kernel.Base.Validation
{
    public class EmailValidator : RegexValidatorBase
    {
        public override string Regex
        {
            get
            {
                return @"^[a-z0-9ñÑ\._\-]+@{1}[a-z0-9ñÑ\-_]+(\.{1}([a-z0-9ñÑ\-_]+))*(\.{1}[a-z0-9ñÑ]{2,}){1}$";
            }
        }

        public override bool Validate(string value)
        {
            return base.Validate(value.ToLower());
        }
    }
}
