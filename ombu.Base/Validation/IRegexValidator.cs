﻿namespace ombu3.Kernel.Base.Validation
{
    public interface IRegexValidator: IGenericValidator <string>
    {
        string Regex { get;  }
    }
}
