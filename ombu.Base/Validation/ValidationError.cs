﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ombu3.Kernel.Base.Base.Validation
{
    public class ValidationError
    {
        public ValidationError(string message) : this(string.Empty, message)
        { }

        public ValidationError(string fieldName, string message)
        {
            this.fieldName = fieldName;
            this.message = message;
        }

        public string fieldName { get; set; }
        public string message { get; set; }

    }
}
