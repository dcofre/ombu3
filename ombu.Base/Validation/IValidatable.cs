﻿namespace ombu3.Kernel.Base.Validation
{
    using ombu3.Kernel.Base.Base.Validation;
    using System.Collections.Generic;

    public interface IValidatable
    {
        void Validate(); 
        IList<ValidationError> ValidationErrors { get;  }
        bool IsValid { get; }
    }
}
