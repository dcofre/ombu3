﻿namespace ombu3.Kernel.Base.Validation
{
    public interface IGenericValidator<T>
    {
        bool Validate(T value);
    }
}
