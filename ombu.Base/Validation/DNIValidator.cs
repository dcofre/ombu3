﻿namespace ombu3.Kernel.Base.Validation
{
    public class DNIValidator : RegexValidatorBase
    {
        public override string Regex
        {
            get
            {
                return @"^\d{6,8}$";
            }
        }
    }
}
