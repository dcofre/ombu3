﻿using ombu3.Kernel.Base.Base.Validation;
using System.Collections.Generic;

namespace ombu3.Kernel.Base.Validation
{
    public abstract class ValidatableValidatorBase : IValidatableValidator
    {
        public void Validate(IValidatable value)
        {
            try
            {
                if (!ExecuteValidation(value))
                {
                    AddError(value, "El valor es inválido");
                }
            }
            catch (System.Exception ex)
            {
                AddError(value, string.Format("Error al validar el valor {0}: {1}", value, ex.Message));
            }
        }

        protected void AddError(IValidatable value, string errDescription)
        {
            value.ValidationErrors.Add(new ValidationError(errDescription));
        }

        protected abstract bool ExecuteValidation(IValidatable value);
    }
}
