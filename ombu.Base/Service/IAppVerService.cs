﻿namespace ombu3.Kernel.Base.Service
{
    public interface IAppVerService
    {
        string DateToShow { get; }
        string Version { get; }
        string VersionId { get; }
    }
}