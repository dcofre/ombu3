﻿using Microsoft.Extensions.Logging;

namespace ombu3.Kernel.Base.Service
{
    public interface IBaseService
    {
        ILogger Logger { get; set; }
    }
}