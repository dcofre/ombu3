﻿using ombu3.Kernel.Base.Base.Exceptions;
using ombu3.Kernel.Base.Base.Validation;
using ombu3.Kernel.Base.DTOs;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ombu3.Kernel.Base.Service
{
    public class BaseService : IBaseService
    {
        public BaseService(BaseServiceDeps dependencies)
        {
            if (dependencies.Logger == null) throw new ArgumentNullException("logger");
            this.Logger = dependencies.Logger;
        }

        public ILogger Logger { get; set; }

        protected void ServiceTryCatch(Action action)
        {
            try
            {
                action.Invoke();
            }
            catch (Exception ex)
            {
                this.ManageException(ex);
            }
        }

        protected void ManageException<TResult>(Exception ex, GenericResultDTO<TResult> result)
        {
            try
            {
                ManageException(ex);
            }
            catch (Exception managedEx)
            {
                if (managedEx is ValidationException)
                {
                    result.Err(((ValidationException)managedEx).Errors.Select(e=> e.message).ToList());
                }
                else
                {
                    result.Err(managedEx.Message);
                }
            }
        }

        protected void ManageException(Exception ex)
        {
            if (ex is AggregateException)
            {
                this.ManageException(ex.InnerException);
            }
            else if (ex is ValidationException
                || ex is EntityNotFoundException
                || ex is System.Threading.ThreadAbortException)
            {
                throw ex;
            }
            else if (ex is ServiceException)
            {
                this.Logger.LogCritical(ex,"ServiceException at BaseService", null);
                throw ex;
            }
            else
            {
                var svcEx = new ServiceException("Other exception", ex);
                this.Logger.LogCritical(ex, "Other exception at BaseService", null);
                throw svcEx;
            }
        }


        protected void DoValidation(IList<ValidationError> errors)
        {
            if (errors.Any())
            {
                throw new ValidationException(errors);
            }
        }

    }
}
