﻿using System.Collections.Generic;
using System.Net.Mail;
using System.Threading.Tasks;

namespace ombu3.Kernel.Base.Service
{
    public interface IMailer
    {
        void Enviar(MailAddress To, string Cc, string Cco, string Subject, string Body);
        void Enviar(MailAddress To, string Cc, string Cco, string ResponderA, string Subject, string Body);
        void Enviar(MailAddress To, string Cc, string Cco, string ResponderA, string Subject, string Body, List<Attachment> attachments);
        Task EnviarAsync(MailAddress To, string Cc, string Cco, string ResponderA, string Subject, string Body, List<Attachment> attachments);
        void EnviarSimple(MailAddress From, MailAddress To, string Cc, string Cco, string Subject, string Body, bool IsBodyHtml, List<Attachment> attachments, string ResponderA, bool TieneDelay, string template);
        bool EsEmail(string inputEmail);
    }
}