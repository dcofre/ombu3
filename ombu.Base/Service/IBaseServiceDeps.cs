﻿using Microsoft.Extensions.Logging;

namespace ombu3.Kernel.Base.Service
{
    public interface IBaseServiceDeps
    {
        ILoggerFactory LoggerFactory { get; }
        ILogger Logger { get; }
    }
}