﻿using Microsoft.Extensions.Logging;

namespace ombu3.Kernel.Base.Service
{
    public class BaseServiceDeps : IBaseServiceDeps
    {
        public BaseServiceDeps(ILoggerFactory loggerFact)
        {
            this.LoggerFactory = loggerFact;
        }

        public ILoggerFactory LoggerFactory { get; private set; }
        public ILogger Logger
        {
            get
            {
                return this.LoggerFactory.CreateLogger(this.GetType().Name);
            }
        }
    }
}
