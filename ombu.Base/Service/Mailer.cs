﻿namespace ombu3.Kernel.Base.Service
{
    using Microsoft.Extensions.Logging;
    using System;
    using System.Collections.Generic;
    using System.Net;
    using System.Net.Mail;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;

    /// <summary>
    /// Aquí se encuentran los métodos para enviar emails y desuscribir
    /// </summary>
    public class Mailer : BaseService, IMailer
    {
        public Mailer(BaseServiceDeps dependencies) : base(dependencies)
        {
        }

        //public Mailer(IConfigurableServiceDeps deps) : base(deps)
        //{
        //    var cfgDict = this.Config.GetAll("Mail");
        //    this.SmtpHost = cfgDict["SmtpHost"].Value;
        //    this.SmtpPort = int.Parse( cfgDict["SmtpPort"].Value);
        //    this.SmtpUser = cfgDict["SmtpUser"].Value;
        //    this.SmtpPass = cfgDict["SmtpPass"].Value;
        //    this.SmtpTimeout = int.Parse(cfgDict["SmtpTimeout"].Value);
        //    this.PopHost = cfgDict["PopHost"].Value;
        //    this.PopPort = int.Parse(cfgDict["PopPort"].Value);
        //    this.PopUser = cfgDict["PopUser"].Value;
        //    this.PopPass = cfgDict["PopPass"].Value;
        //    this.FromMail = cfgDict["FromMail"].Value;
        //    this.FromNombre = cfgDict["FromNombre"].Value;
        //    this.DelayEnvio = int.Parse(cfgDict["DelayEnvio"].Value);
        //}

        protected string SmtpHost { get; set; }
        protected int SmtpPort { get; set; }
        protected string SmtpUser { get; set; }
        protected string SmtpPass { get; set; }
        protected int SmtpTimeout { get; set; }
        protected string PopHost { get; set; }
        protected int PopPort { get; set; }
        protected string PopUser { get; set; }
        protected string PopPass { get; set; }
        protected string FromMail { get; set; }
        protected string FromNombre { get; set; }
        protected int DelayEnvio { get; set; }


        public bool EsEmail(string inputEmail)
        {
            string strRegex = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
                              @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
                              @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
            var re = new Regex(strRegex);
            if (re.IsMatch(inputEmail))
                return true;
            else
                return false;
        }

        public void EnviarSimple(MailAddress From, MailAddress To, string Cc, string Cco, string Subject, string Body, bool IsBodyHtml, List<Attachment> attachments, string ResponderA, bool TieneDelay, string template)
        {
            try
            {
                var Mail = new MailMessage();
                Mail.From = From;
                Mail.To.Add(To);
                if (!string.IsNullOrEmpty(ResponderA))
                    Mail.ReplyTo = new MailAddress(ResponderA);
                else
                    Mail.ReplyTo = new MailAddress(Mail.From.Address);
                Mail.Subject = Subject;

                if (template != null && template.Length > 0)
                    Mail.Body = template.Replace("{body}", Body);
                else
                    Mail.Body = Body;

                Mail.IsBodyHtml = IsBodyHtml;
                Mail.BodyEncoding = Encoding.UTF8;

                if (Cc.Length > 0)
                {
                    String[] copies = Cc.Split(',');
                    foreach (String copy in copies)
                    {
                        Mail.CC.Add(new MailAddress(copy));
                    }
                }

                if (Cco.Length > 0)
                {
                    String[] copies = Cco.Split(',');
                    foreach (String copy in copies)
                    {
                        Mail.Bcc.Add(new MailAddress(copy));
                    }
                }

                foreach (Attachment att in attachments) Mail.Attachments.Add(att);
                var smtp = new SmtpClient(this.SmtpHost, this.SmtpPort);
                smtp.Timeout = this.SmtpTimeout;
                smtp.Credentials = new NetworkCredential(this.SmtpUser, this.SmtpPass);
                smtp.EnableSsl = true;
                smtp.Send(Mail);
                //enviado = true;
                if (TieneDelay) System.Threading.Thread.Sleep(this.DelayEnvio);
            }
            catch (Exception ex)
            {
                this.Logger.LogError("Error enviando mail a: " + To.Address + "; subject: " + Subject + ";u:" + this.SmtpUser + ";p: " + this.SmtpPass + ";port:" + this.SmtpPort, ex);
                throw;
            }
        }

        public void Enviar(MailAddress To, string Cc, string Cco, string Subject, string Body)
        {
            Enviar(To, Cc, Cco, "", Subject, Body, new List<Attachment>());
        }

        public void Enviar(MailAddress To, string Cc, string Cco, string ResponderA, string Subject, string Body)
        {
            Enviar(To, Cc, Cco, ResponderA, Subject, Body, new List<Attachment>());
        }

        public async Task EnviarAsync(MailAddress To, string Cc, string Cco, string ResponderA, string Subject, string Body, List<Attachment> attachments)
        {
            await Task.Run(() => Enviar(To, Cc, Cco, ResponderA, Subject, Body, attachments));
        }

        public void Enviar(MailAddress To, string Cc, string Cco, string ResponderA, string Subject, string Body, List<Attachment> attachments)
        {
            try
            {
                MailMessage Mail = new MailMessage();
                Mail.From = new MailAddress(this.FromMail, this.FromNombre);
                Mail.To.Add(To);
                Mail.ReplyTo = (!String.IsNullOrEmpty(ResponderA)) ? new MailAddress(ResponderA) : new MailAddress(Mail.From.Address);
                Mail.Subject = Subject;
                Mail.Body = Body;
                Mail.IsBodyHtml = true;
                Mail.BodyEncoding = Encoding.UTF8;

                if (Cc.Length > 0)
                {
                    String[] copies = Cc.Split(',');
                    foreach (String copy in copies)
                        Mail.CC.Add(new MailAddress(copy));
                }

                if (Cco.Length > 0)
                {
                    String[] copies = Cco.Split(',');
                    foreach (String copy in copies)
                    {
                        Mail.Bcc.Add(new MailAddress(copy));
                    }
                }

                if (attachments != null)
                {
                    foreach (Attachment att in attachments)
                    {
                        Mail.Attachments.Add(att);
                    }
                }

                var smtp = new SmtpClient(this.SmtpHost, this.SmtpPort);
                smtp.Timeout = this.SmtpTimeout;
                smtp.EnableSsl = true;
                smtp.Credentials = new NetworkCredential(this.SmtpUser, this.SmtpPass);
                smtp.Send(Mail);
            }
            catch (Exception ex)
            {
                this.Logger.LogError("Error enviando mail a: " + To.Address + "; subject: " + Subject + ";u:" + this.SmtpUser + ";p: " + this.SmtpPass, ex);
                throw;
            }
        }
    }
}
