﻿using System;
using System.Text.RegularExpressions;

namespace ombu3.Kernel.Base.Format
{
    public class CUIT : IFormatProvider, ICustomFormatter
    {

        public object GetFormat(Type formatType)
        {
            if (formatType == typeof(ICustomFormatter))
            {
                return this;
            }
            return null;
        }

        public string Format(string format, object arg, IFormatProvider formatProvider)
        {

            if (arg == null || string.IsNullOrEmpty(arg.ToString()) || arg.ToString() == "0")
            {
                return string.Empty;
            }

            var validatePattern = @"^\d{9,11}$";

            try
            {
                var toFormat = arg.ToString();


                var reg = new Regex(validatePattern);
                if (!reg.IsMatch(toFormat))
                {
                    return string.Empty;
                }


                var first = toFormat.Substring(0,2);
                var last = toFormat.Substring(toFormat.Length - 1);

                var middle = toFormat.Substring(2, toFormat.Length - 3);
                var result = string.Format("{0}-{1}-{2}", first, middle, last);

                return result;

            }
            catch (FormatException e)
            {
                throw new FormatException(string.Format("The format of '{0}' is invalid.", format), e);
            }
        }
    }
}
