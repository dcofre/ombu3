﻿using System;
using System.Linq;
using System.Reflection;

namespace ombu3.Kernel.Base.Format
{
    public static class FormatFactory
    {

        /// <summary>
        /// Devuelve una instancia con el formatprovider correspondiente.
        /// </summary>
        /// <param name="format"></param>
        /// <returns></returns>
        public static IFormatProvider Get(string format)
        {
            try
            {
                var currentAssembly = Assembly.GetExecutingAssembly();
                var currentType = currentAssembly.GetTypes().SingleOrDefault(t => t.Name.ToUpper().EndsWith(format.ToUpper()));
                return (IFormatProvider)Activator.CreateInstance(currentType);
            }
            catch (Exception ex)
            {
                throw new FormatException(string.Format("Formato [{0}] no reconocido", format), ex);
            }

        }
    }
}
