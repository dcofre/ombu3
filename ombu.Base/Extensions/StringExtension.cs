﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace ombu3.Kernel.Base.Base.Extensions
{
    public static class StringExtension
    {
        public static string SubstringSafe(this string str, int startIndex)
        {
            return SubstringSafe(str, startIndex, str.Length);
        }

        public static string SubstringSafe(this string str,int startIndex,int length)
        {
            try
            {
                startIndex = startIndex < 0 ? 0 : startIndex;
                length = length < 0 ? str.Length : length;

                if (startIndex + length <= str.Length)
                {
                    return str.Substring(startIndex, length);
                }
                else
                {
                    return str.Substring(startIndex);
                }
            }
            catch 
            {
                return str;
            }

        }
    }
}
