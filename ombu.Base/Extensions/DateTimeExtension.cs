﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ombu3.Kernel.Base.Base.Extensions
{
    public static class DateTimeExtension
    {
        public static int Edad(this DateTime birthDate)
        {
            return getEdad(birthDate, null);
        }

        public static int Edad(this DateTime birthDate,DateTime toCompare)
        {
            return getEdad(birthDate, toCompare);
        }

        private static int getEdad(DateTime birthDate, DateTime? toCompare)
        {
            if(toCompare == null)
            {
                toCompare = DateTime.Today;
            }

            int age = toCompare.Value.Year - birthDate.Year;
            if (toCompare.Value.Month < birthDate.Month || (toCompare.Value.Month == birthDate.Month && toCompare.Value.Day < birthDate.Day))
                age--;
            return age;
        }


    }
}
