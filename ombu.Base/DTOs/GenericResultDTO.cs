﻿namespace ombu3.Kernel.Base.DTOs
{
    public class GenericResultDTO<TResult> : ResultDTO
    {
        public GenericResultDTO()
        {
            this.Status = eResultStatus.Unknown;
        }

        public GenericResultDTO(eResultStatus status, string message) : this()
        {
            this.Status = status;
            this.Message = message;
        }

        public void Ok(TResult result)
        {
            this.Ok(result, string.Empty);
        }

        public void Ok(TResult result, string message)
        {
            this.Result = result;
            this.Status = eResultStatus.Ok;
            this.Message = message;
        }

        public void Warn(TResult result)
        {
            this.Warn(result, string.Empty);
        }

        public void Warn(TResult result, string message)
        {
            this.Result = result;
            this.Status = eResultStatus.Warning;
            this.Message = message;
        }

        public TResult Result { get; set; }

    }
}

