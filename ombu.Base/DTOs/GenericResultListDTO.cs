﻿namespace ombu3.Kernel.Base.DTOs
{
    using System.Collections.Generic;
    using System.Linq;

    public class GenericResultListDTO<TResult>
    {
        private List<GenericResultDTO<TResult>> _list;

        public GenericResultListDTO()
        {
            _list = new List<GenericResultDTO<TResult>>();
        }

        public IList<GenericResultDTO<TResult>> Results
        {
            get
            {
                return _list;
            }
        }

        public void AddResult(eResultStatus status)
        {
            this.AddResult(status, null);
        }

        public void AddResult(eResultStatus status, string message)
        {
            this._list.Add(new GenericResultDTO<TResult> (status, message));
        }

        public bool IsOk()
        {
            return _list.Count() == _list.Where(l => l.Status == eResultStatus.Ok).Count();
        }

    }
}

