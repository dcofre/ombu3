﻿namespace ombu3.Kernel.Base.DTOs
{
    public class NameValueDTO
    {
        public NameValueDTO(string name, string value)
        {
            this.Name = name;
            this.Value = value;
        }

        public string Name { get; set; }
        public string Value { get; set; }
    }
}
