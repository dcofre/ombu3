﻿namespace ombu3.Kernel.Base.DTOs
{
    using System;

    public abstract class PaginatedRequestDTO : BaseDTO
    {
        public PaginatedRequestDTO()
        {
            this.ResultsPerPage = 25;
        }

        public int PageNumber { get; set; }
        public int ResultsPerPage { get; set; }
        public int? FirstRow
        {
            get
            {
                if (this.PageNumber > 0)
                {
                    return ((this.PageNumber - 1) * this.ResultsPerPage) + 1;
                }
                else
                {
                    return null;
                }
            }
        }

        public int? LastRow
        {
            get
            {
                var frow = this.FirstRow;
                if (frow != null)
                {
                    return frow.Value + this.ResultsPerPage - 1;
                }
                else
                {
                    return null;
                }
            }
        }

    }
}
