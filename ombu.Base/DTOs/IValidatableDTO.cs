﻿
using ombu3.Kernel.Base.Base.Validation;
using System.Collections.Generic;

namespace ombu3.Kernel.Base.DTOs
{
    public interface IValidatableDTO : IDTO
    {
        bool Validate { get; set; }
    }
}
