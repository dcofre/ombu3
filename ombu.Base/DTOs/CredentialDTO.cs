﻿using bt.Kernel.Base.Validation;

namespace bt.Kernel.Base.DTOs
{
    public class CredentialDTO
    {
        public virtual string EmailOrId { get; set; }

        public virtual string Password { get; set; }

        public virtual string Email
        {
            get
            {
                if (this.HasEmail())
                {
                    return this.EmailOrId;
                }
                else
                {
                    return null;
                }
            }
        }

        public virtual string Id
        {
            get
            {
                if (this.HasId())
                {
                    return this.EmailOrId;
                }
                else
                {
                    return null;
                }
            }
        }

        protected virtual bool HasId()
        {
            return !this.HasEmail();
        }

        protected virtual bool HasEmail()
        {
            var eVal = new EmailValidator();

            return eVal.Validate(this.EmailOrId);
        }

    }
}
