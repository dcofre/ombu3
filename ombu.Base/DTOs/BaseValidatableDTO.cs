﻿using ombu3.Kernel.Base.Base.Validation;
using System.Collections.Generic;

namespace ombu3.Kernel.Base.DTOs
{
    public abstract class BaseValidatableDTO : BaseDTO, IValidatableDTO
    {
        public bool Validate { get; set; }
    }
}
