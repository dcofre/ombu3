﻿namespace ombu3.Kernel.Base.DTOs
{
    using System.Collections.Generic;
    using System.Linq;

    public enum eResultStatus
    {
        Unknown,
        Ok,
        Warning,
        Error
    }

    public class ResultDTO : BaseDTO
    {
        public ResultDTO(eResultStatus status, string message) : this()
        {
            this.Status = status;
            this.Message = message;
        }

        public ResultDTO()
        {
            this.Status = eResultStatus.Unknown;
            this.ValidationErrors = new List<string>();
        }

        public virtual void Err(string errorMessage)
        {
            this.Err(errorMessage, null);
        }

        public virtual void Err(IList<string> errors)
        {
            if (errors != null && errors.Any())
            {
                string msg = (errors.Count == 1 ? errors.First() : "Se produjeron errores al validar");
                this.Err(msg, errors);
            }
        }

        public virtual void Err(string message, IList<string> errors)
        {
            this.Status = eResultStatus.Error;
            this.Message = message;
            this.ValidationErrors = errors;
        }

        public virtual void Ok(object result)
        {
            this.Ok(result, string.Empty);
        }

        public virtual void Ok(object result, string message)
        {
            this.Result = result;
            this.Status = eResultStatus.Ok;
            this.Message = message;
        }

        public virtual void Warn(object result)
        {
            this.Warn(result, string.Empty);
        }

        public virtual void Warn(object result, string message)
        {
            this.Result = result;
            this.Status = eResultStatus.Warning;
            this.Message = message;
        }

        public virtual object Result { get; set; }
        public eResultStatus Status { get; set; }
        public string Message { get; set; }
        public IList<string> ValidationErrors { get; set; }

    }
}

