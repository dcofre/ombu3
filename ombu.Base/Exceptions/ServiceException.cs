﻿using System;
using System.Collections.Generic;

namespace ombu3.Kernel.Base.Base.Exceptions
{
    [Serializable]
    public class ServiceException : Exception
    {
        public ServiceException(string msg = null, Exception innerEx = null)
            : base(msg, innerEx)
        {
            this.Errors = new List<string>();
            if (!String.IsNullOrEmpty(msg))
            {
                this.Errors.Add(msg);
            }
        }

        public ServiceException(IList<string> errors, string msg)
            : this(msg)
        {
            this.Errors.AddRange(errors);
        }

        public List<string> Errors { get; set; }
    }
}
