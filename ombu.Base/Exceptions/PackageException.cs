﻿using System;
using System.Collections.Generic;

namespace ombu3.Kernel.Base.Base.Exceptions
{
    [Serializable]
    public class PackageException : Exception
    {
        public PackageException() { }
        public PackageException(string message) : base(message) { }
        public PackageException(string message, Exception inner) : base(message, inner) { }
        protected PackageException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context)
        { }

        public PackageException(List<string> errores)
            : this("Han ocurrido errores en el paquete.")
        {
            this.Errores = errores;
        }

        public List<string> Errores { get; set; }
    }
}
