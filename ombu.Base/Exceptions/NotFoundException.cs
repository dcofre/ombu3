﻿namespace ombu3.Kernel.Base.Base.Exceptions
{
    using System;

    [Serializable()]
    public class NotFoundException : ClientException
    {
        public NotFoundException(string msg = null, Exception innerEx = null)
            : base(404, msg, innerEx)
        { }
    }
}
