﻿using System;

namespace ombu3.Kernel.Base.Base.Exceptions
{
    [Serializable()]
    public class SessionNullException : Exception
    {
        public SessionNullException() : base("La httpSession es nula")
        { }

        public SessionNullException(string msg) : base(msg)
        { }
    }
}
