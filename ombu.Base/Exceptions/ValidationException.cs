﻿namespace ombu3.Kernel.Base.Base.Exceptions
{
    using ombu3.Kernel.Base.Base.Validation;
    using System;
    using System.Collections.Generic;

    [Serializable]
    public class ValidationException : Exception
    {
        public ValidationException(string message) : base(message)
        {
            this.Errors = new List<ValidationError>();
        }

        public ValidationException(IList<ValidationError> errors) : base(string.Format("{0} validation errors has ocurred. Check errors list for details", errors.Count))
        {
            this.Errors = errors;
        }

        public IList<ValidationError> Errors { get; set; }
    }
}
