﻿using System;

namespace ombu3.Kernel.Base.Base.Exceptions
{
    [Serializable()]
    public class ClientException : Exception
    {
        public int Code { get; protected set; }

        public ClientException(string msg) : this(400, msg, null)
        { }

        public ClientException(string msg, Exception innerEx) : this(500, msg, innerEx)
        { }

        public ClientException(int code, string msg, Exception innerEx) : base(msg, innerEx)
        {
            this.Code = code;
        }
    }
}
