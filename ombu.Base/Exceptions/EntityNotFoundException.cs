﻿using System;

namespace ombu3.Kernel.Base.Base.Exceptions
{
    using System;

    public class EntityNotFoundException : Exception
    {
        public EntityNotFoundException(Exception innerException) :
            base("No se encontró la entidad solicitada", innerException)
        { }
    }
}
