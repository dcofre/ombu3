﻿namespace ombu3.Kernel.Base.Utils
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public static class Transformer
    {
        public static string ListToString(IList<string> list)
        {
            StringBuilder strBuild = new StringBuilder();
            foreach (string str in list)
            {
                strBuild.AppendLine(str);
            }
            return strBuild.ToString();
        }

        public static double ToUnixTimestamp(DateTime date)
        {
            DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            TimeSpan diff = date.ToUniversalTime() - origin;
            return Math.Floor(diff.TotalSeconds);
        }
    }
}
