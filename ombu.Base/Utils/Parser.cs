﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ombu3.Kernel.Base.Utils
{
    public static class Parser
    {
        public static Guid? ParseGuid(string id)
        {
            Guid? retVal = null;
            try
            {
                retVal = new Guid(id);
            }
            catch
            {
                //no loguea a proposito
            }
            return retVal;
        }

        public static Dictionary<string, string> ToNameValueDictionary(string strparameters)
        {
            var retVal = new Dictionary<string, string>();
            if (!string.IsNullOrEmpty(strparameters))
            {
                var pars = strparameters.Split(',');
                foreach (var p in pars)
                {
                    var keyval = p.Split('=');
                    if (keyval.Length == 2)
                    {
                        retVal.Add(keyval.First().Trim(), keyval.Last().Trim());
                    }
                }
            }
            return retVal;
        }



    }
}
