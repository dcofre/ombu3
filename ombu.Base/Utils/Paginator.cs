﻿namespace ombu3.Kernel.Base.Utils
{
    using System;

    public class Paginator
    {
        public int RowsPerPage { get; set; }
        public int TotalRowCount { get; set; }

        public int TotalPageCount
        {
            get
            {
                return (int)Math.Ceiling((double)this.TotalRowCount / this.RowsPerPage);
            }
        }

        public int CalculatePage(int rowNum)
        {
            if (rowNum <= this.RowsPerPage)
            {
                return 1;
            }
            else
            {
                return (int)Math.Ceiling((double)rowNum / this.RowsPerPage);
            }
        }

        public int CalculateRow(int pageNum)
        {
            if (pageNum > 0 )
            {
                return ((pageNum - 1) * this.RowsPerPage)  +1;
            }
            else
            {
                return 0;
            }
        }
    }
}
