﻿using System.Reflection;

namespace ombu3.Kernel.Base.Utils
{
    public class Reflector
    {
        protected Reflector()
        { }

        private  Reflector _singleton;
        public  Reflector Get()
        {
            if (_singleton == null)
            {
                _singleton = new Reflector();
            }
            return _singleton;
        }

        public virtual Assembly Assembly
        {
            get
            {
                return System.Reflection.Assembly.GetExecutingAssembly();
            }
        }

    }
}
