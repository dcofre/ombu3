﻿namespace ombu3.Kernel.Base.Utils
{
    using System;
    using System.Diagnostics;

    public static class SystemHelper
    {
        public static string RevealCaller(int deep)
        {
            var frame = new StackTrace().GetFrame(1 + deep);
            var cls = frame.GetMethod().ReflectedType.Name;
            var methodName = frame.GetMethod().Name;
            return string.Format("{0}.{1}", cls, methodName);
        }
    }
}
