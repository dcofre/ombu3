﻿using ombu3.Kernel.Base.DTOs;
using ombu3.Kernel.DAL;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;

namespace ombu3.Kernel.Web
{
    public abstract class ReadonlyRESTController<TDTO, TId> : BaseApiController where TDTO : IDTO
    {
        protected ICRUDService<TDTO, TId> service;

        public ReadonlyRESTController( ICRUDService<TDTO, TId> service) : base( )
        {
            this.service = service;
        }

        [HttpGet("api/[controller]")]
        [ProducesResponseType(typeof(IDictionary<string, string>), 400), ProducesResponseType(typeof(void), 400), ProducesResponseType(typeof(void), 404), ProducesResponseType(typeof(void), 409)]
        public virtual ActionResult Get()
        {
            try
            {
                IList<TDTO> dto = this.service.ReadAll();
                return Ok(dto);
            }
            catch (Exception ex)
            {
                return (this.ToResult(ex));
            }
        }

        [HttpGet("api/[controller]/{id}")]
        [ProducesResponseType(typeof(IDictionary<string, string>), 400), ProducesResponseType(typeof(void), 400), ProducesResponseType(typeof(void), 404)]
        public virtual ActionResult Get(TId id)
        {
            try
            {
                TDTO dto = this.service.Read(id);

                if (dto == null)
                {
                    return NotFound();
                }
                else
                {
                    return Ok(dto);
                }
            }
            catch (Exception ex)
            {
                return (this.ToResult(ex));
            }
        }

    }
}
