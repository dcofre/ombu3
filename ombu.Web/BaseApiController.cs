﻿namespace ombu3.Kernel.Web
{
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;

    [Route("api/[controller]")]
    public class BaseApiController : BaseController
    {
        public BaseApiController() : base()
        { }
    }
}
