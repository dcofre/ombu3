﻿namespace ombu3.Kernel.Web
{
    using Microsoft.AspNetCore.Mvc;
    using NLog;
    using ombu3.Kernel.Base.Base.Exceptions;
    using ombu3.Kernel.Base.Base.Validation;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security;

    [ProducesResponseType(typeof(IList<ValidationError>), 400)]
    public class BaseController : Controller
    {
        protected ILogger Logger { get; set; }

        protected ActionResult ToResult(object result, string createdAt = null)
        {
            ActionResult retVal;
            if (result == null)
            {
                retVal = NotFound();
            }
            else if (!string.IsNullOrEmpty(createdAt))
            {
                retVal = Created(this.Request.HttpContext.GetHost() + createdAt, result);
            }
            else
            {
                retVal = Ok(result);
            }

            return retVal;
        }

        protected ActionResult ToResult(Exception ex)
        {
            ActionResult retVal;
            if (ex is ValidationException)
            {
                var vex = ex as ValidationException;

                if (vex.Errors.Any())
                {
                    retVal = BadRequest(vex.Errors);
                }
                else
                {
                    retVal = BadRequest(vex.Message);
                }
            }
            else if (ex.InnerException is ValidationException)
            {
                var vex = ex.InnerException as ValidationException;
                retVal = BadRequest(vex.Errors);
            }
            else if (ex is ArgumentException || ex.InnerException is ArgumentException
                    || ex is ArgumentNullException || ex.InnerException is ArgumentNullException)
            {
                retVal = BadRequest(ex.Message);
            }
            else if (ex is SecurityException)
            {
                retVal = Unauthorized();
            }
            else if (ex is EntityNotFoundException)
            {
                retVal = NotFound();
            }
            else
            {
                this.Logger.Error(ex, "Error captured by basecontroller", null);
                retVal = Err();
            }

            // return StatusCode(406, "There was a problem saving record in the database. Please try again.");
            //400 Bad Request -The request is malformed, such as message body format error.
            //401 Unauthorized - Wrong or no authencation ID/ password provided.
            //403 Forbidden - It's used when the authentication succeeded but authenticated user doesn't have permission to the request resource
            //404 Not Found -When a non-existent resource is requested
            //405 Method Not Allowed - The error checking for unexpected HTTP method.For example, the RestAPI is expecting HTTP GET, but HTTP PUT is used.
            //429 Too Many Requests - The error is used when there may be DOS attack detected or the request is rejected due to rate limiting

            return retVal;
        }

        protected ActionResult Err()
        {
            return StatusCode(500);
        }
    }
}
