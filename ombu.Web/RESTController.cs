﻿using ombu3.Kernel.Base.DTOs;
using ombu3.Kernel.DAL;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using NLog;

namespace ombu3.Kernel.Web
{
    public abstract class RESTController<TDTO, TId> : InsertUpdateRESTController<TDTO,TId> where TDTO : IDTO
    {

        public RESTController(ILogger logger, ICRUDService<TDTO, TId> service) : base(logger, service)
        { }

        [HttpDelete("api/[controller]/{id}")]
        [ProducesResponseType(typeof(IDictionary<string, string>), 400), ProducesResponseType(typeof(void), 400), ProducesResponseType(typeof(void), 404), ProducesResponseType(typeof(void), 409)]
        public virtual ActionResult Delete(TId id)
        {
            try
            {
                this.service.Delete(id);
                return Ok();
            }
            catch (Exception ex)
            {
                return (this.ToResult(ex));
            }
        }
    }
}
