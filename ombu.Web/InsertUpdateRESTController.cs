﻿using ombu3.Kernel.Base.DTOs;
using ombu3.Kernel.DAL;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using NLog;

namespace ombu3.Kernel.Web
{
    public abstract class InsertUpdateRESTController<TDTO, TId> : ReadonlyRESTController<TDTO, TId> where TDTO : IDTO
    {
        public InsertUpdateRESTController(ILogger logger, ICRUDService<TDTO, TId> service) : base(service)
        { }

        [HttpPost("api/[controller]")]
        [ProducesResponseType(typeof(IDictionary<string, string>), 400), ProducesResponseType(typeof(void), 400), ProducesResponseType(typeof(void), 404), ProducesResponseType(typeof(void), 409)]
        public virtual ActionResult Post([FromBody]TDTO value)
        {
            try
            {
                TDTO dto = this.service.Insert(value);

                if (dto == null)
                {
                    return NotFound();
                }
                else
                {
                    return Ok(dto);
                }
            }
            catch (Exception ex)
            {
                return (this.ToResult(ex));
            }
        }

        [HttpPut("api/[controller]")]
        [ProducesResponseType(typeof(IDictionary<string, string>), 400), ProducesResponseType(typeof(void), 400), ProducesResponseType(typeof(void), 404), ProducesResponseType(typeof(void), 409)]
        public virtual ActionResult Put([FromBody]TDTO value)
        {
            try
            {
                var dto = this.service.Update(value);
                if (dto != null)
                {
                    return Ok();
                }
                else
                {
                    return NotFound();
                }
            }
            catch (Exception ex)
            {
                return (this.ToResult(ex));
            }
        }

    }
}
