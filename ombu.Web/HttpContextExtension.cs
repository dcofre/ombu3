﻿using Microsoft.AspNetCore.Http;
using System;

namespace ombu3.Kernel.Web
{
    public static class HttpContextExtension
    {
        public static string UserHostAddress(this HttpContext http)
        {
            return http.Connection.RemoteIpAddress.ToString();
        }

        public static string UserHostName(this HttpContext http)
        {
            return http.Request.Host.Value;
        }

        public static string UserAgent(this HttpContext http)
        {
            return http.Request.Headers["User-Agent"].ToString();
        }

        public static string UrlReferrer(this HttpContext http)
        {
            return http.Request.Headers["Referer"].ToString();
        }

        public static string GetHeaderOrCookieValue(this HttpContext http, string key)
        {
            return HttpContextExtension.GetHeaderValue(http, key) ?? HttpContextExtension.GetCookieValue(http, key);
        }

        public static string GetHeaderValue(this HttpContext http, string key)
        {
            return http.Request.Headers[key].ToString();
        }

        public static string GetCookieValue(this HttpContext http, string key)
        {
            return http.Request.Cookies[key].ToString();
        }

        public static Uri GetHost(this HttpContext http)
        {
            UriBuilder uriBuilder;

            if (http.Request.Host.Port == null)
            {
                uriBuilder = new UriBuilder(http.Request.Scheme, http.Request.Host.Host);
            }
            else
            {
                uriBuilder = new UriBuilder(http.Request.Scheme, http.Request.Host.Host, http.Request.Host.Port.Value);
            }

            return uriBuilder.Uri;
        }

        public static Uri GetAbsoluteUri(this HttpContext http)
        {
            UriBuilder uriBuilder = new UriBuilder(GetHost(http));
            uriBuilder.Path = http.Request.Path.Value;
            uriBuilder.Query = http.Request.QueryString.ToString();
            return uriBuilder.Uri;
        }

    }
}
