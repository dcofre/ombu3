﻿namespace ombu3.Kernel.DAL
{
    using ombu3.Kernel.Base.Service;
    using Microsoft.Extensions.Logging;

    public class DBServiceDeps : ConfigurableServiceDeps, IDBServiceDeps
    {
        public DBServiceDeps(ILoggerFactory loggerFact, IConfigProvider config, IUOW uow, IMapperService mapper, IRepository repo) : base(new BaseServiceDeps(loggerFact), config)
        {
            this.UoW = uow;
            this.Mapper = mapper;
            this.Repo = repo;
        }

        public IUOW UoW { get; set; }
        public IMapperService Mapper { get; set; }
        public IRepository Repo { get; set; }

    }
}
