﻿using ombu3.Kernel.Base.DTOs;
using ombu3.Kernel.DAL.Entities;
using System.Collections.Generic;

namespace ombu3.Kernel.DAL
{
    public interface IMapperService
    {
        IList<TDTO> ToDTO<TDTO, TEntity>(IList<TEntity> entList) where TDTO : IDTO where TEntity : IEntity;
        TDTO ToDTO<TDTO, TEntity>(TEntity ent) where TDTO : IDTO where TEntity : IEntity;
        IList<TEntity> ToEntity<TDTO, TEntity>(IList<TDTO> entList) where TDTO : IDTO where TEntity : IEntity;
        TEntity ToEntity<TDTO, TEntity>(TDTO dto) where TDTO : IDTO where TEntity : IEntity;
    }
}