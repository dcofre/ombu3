﻿using ombu3.Kernel.Base.Service;

namespace ombu3.Kernel.DAL
{
    public interface IConfigurableService: IBaseService
    {
        IConfigProvider Config { get; set; }
    }
}