﻿namespace ombu3.Kernel.DAL
{
    using ombu3.Kernel.Base.Service;
    using Microsoft.Extensions.Logging;
    using System;

    public class ConfigurableService : BaseService, IConfigurableService
    {
        public ConfigurableService(IConfigurableServiceDeps deps) : this(deps.LoggerFactory, deps.Config)
        {
            if (deps == null) throw new ArgumentNullException("deps");
        }

        protected ConfigurableService(ILoggerFactory loggerFact, IConfigProvider config) : base(new BaseServiceDeps(loggerFact))
        {
            if (config == null) throw new ArgumentNullException("config");
            this.Config = config;
        }

        public IConfigProvider Config { get; set; }
    }
}
