﻿namespace ombu3.Kernel.DAL
{
    using ombu3.Kernel.DAL.Entities;
    using System.Collections.Generic;

    public interface IConfigProvider
    {
        string ModuleId { get; }
        /// <summary>
        /// Obtiene el registro de configuración correspondiente a la key
        /// </summary>
        /// <returns></returns>
        Config Get(string key);
        /// <summary>
        /// Obtiene el registro de configuración correspondiente a la key y al módulo especificado.
        /// </summary>
        /// <returns></returns>
        Config Get(string key, string moduleId);
        /// <summary>
        /// Obtiene el listado de registros de configuracion
        /// </summary>
        /// <returns></returns>
        IDictionary<string, Config> GetAll();

        IDictionary<string, Config> GetAll(string moduleId);

        Config Upsert(Config dto);
    }
}
