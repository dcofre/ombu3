﻿
namespace ombu3.Kernel.DAL
{
    using ombu3.Kernel.Base.DTOs;
    using ombu3.Kernel.DAL.Entities;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public abstract class CRUDService<TDTO, TEntity, TId> : BaseDBService, ICRUDService<TDTO, TId>
    where TDTO : IDTO
    where TEntity : class, IEntity<TId>
    {
        public CRUDService(IDBServiceDeps deps) : base(deps)
        {
        }

        /// <summary>
        /// Obtiene un DTO por id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public virtual TDTO Read(TId id)
        {
            TDTO retVal = default(TDTO);
            try
            {
                var entity = this.Repo.GetOne<TEntity>(id);
                if (entity != null) retVal = this.MapSvc.ToDTO<TDTO, TEntity>(entity);
            }
            catch (Exception ex)
            {
                this.ManageException(ex);
            }
            return retVal;
        }

        /// <summary>
        /// Obtiene el listado completo de dtos
        /// </summary>
        /// <returns></returns>
        public virtual IList<TDTO> ReadAll()
        {
            try
            {
                var entList = this.Repo.GetAll<TEntity>();
                return entList.Select(e => this.MapSvc.ToDTO<TDTO, TEntity>(e)).ToList();
            }
            catch (Exception ex)
            {
                this.ManageException(ex);
                throw;
            }
        }

        /// <summary>
        /// Actualiza el dto
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public virtual TDTO Insert(TDTO dto)
        {
            TDTO res = default(TDTO);
            try
            {
                var ent = this.Repo.Insert(this.MapSvc.ToEntity<TDTO, TEntity>(dto));
                if (ent != null) res = this.MapSvc.ToDTO<TDTO, TEntity>(ent);
            }
            catch (Exception ex)
            {
                this.ManageException(ex);
            }
            return res;
        }

        /// <summary>
        /// Actualiza el dto
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public virtual TDTO Update(TDTO dto)
        {
            TDTO res = default(TDTO);
            try
            {
                var ent = this.Repo.Update(this.MapSvc.ToEntity<TDTO, TEntity>(dto));
                if (ent != null) res = this.MapSvc.ToDTO<TDTO, TEntity>(ent);
            }
            catch (Exception ex)
            {
                this.ManageException(ex);
            }
            return res;
        }

        /// <summary>
        /// Actualiza el dto
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public virtual TDTO Upsert(TDTO dto)
        {
            TDTO res = default(TDTO);
            try
            {
                var ent = this.Repo.Upsert(this.MapSvc.ToEntity<TDTO,TEntity>(dto));
                if (ent != null) res = this.MapSvc.ToDTO<TDTO, TEntity>(ent);
            }
            catch (Exception ex)
            {
                this.ManageException(ex);
            }
            return res;
        }

        /// <summary>
        /// Borra el dto
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public virtual void Delete(TId id)
        {
            try
            {
                this.Repo.Delete<TEntity>(id);
            }
            catch (Exception ex)
            {
                this.ManageException(ex);
            }
        }
    }
}
