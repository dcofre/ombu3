﻿using System;
using System.Collections.Generic;
using System.Data;

namespace ombu3.Kernel.DAL.Helpers
{
    public static class DbHelper
    {
        public static void LoadDataTable(DataTable dt, SelectHelper query, IDbConnection cnx)
        {
            IDataReader result = null;
            try
            {
                result = DbHelper.ExecuteReader(query, cnx);
                dt.Load(result);
            }
            catch
            {
                throw;
            }
            finally
            {
                if (result != null) result.Close();
            }

        }

        public static void LoadDataTable(DataTable dt, string commandText, IDictionary<string, object> pars, IDbConnection cnx, CommandType cmdType = CommandType.StoredProcedure)
        {
            IDataReader result = null;
            try
            {
                result = DbHelper.ExecuteReader(commandText, pars, cnx, cmdType);
                dt.Load(result);
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                if (result != null) result.Close();
            }

        }


        public static bool LoadData(Action<IDataReader> loadAction, SelectHelper query, IDbConnection cnx)
        {
            bool retVal = false;
            IDataReader dr = null;
            try
            {
                dr = DbHelper.ExecuteReader(query, cnx);

                if (loadAction != null)
                {
                    while (dr.Read())
                    {
                        loadAction.Invoke(dr);
                    }
                }
                retVal = true;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (dr != null) dr.Close();
                dr.Dispose();
            }

            return retVal;
        }

        public static int ExecuteNonQuery(string commandtext, IDictionary<string, object> parameters, bool return_last_id, IDbConnection conn)
        {
            int response = 0;
            if (return_last_id == true)
            {
                // query.NonQuery += "; SELECT CAST(scope_identity() AS int)"; //si quiero que me devuelva el ultimo id insertado ejecuto este query
            }

            using (var comm = conn.CreateCommand())
            {
                comm.CommandText = commandtext;
                comm.CommandType = CommandType.Text;

                if (parameters != null) AddParameters(comm, parameters);

                if (return_last_id)
                {
                    response = (int)comm.ExecuteScalar();
                }
                else
                {
                    response = comm.ExecuteNonQuery();
                }
            }

            return response;
        }

        private static void AddParameters(IDbCommand comm, IDictionary<string, object> pars)
        {
            if (pars != null)
            {
                foreach (KeyValuePair<string, object> param in pars)
                {
                    var tmpPar = comm.CreateParameter();
                    tmpPar.ParameterName = param.Key;
                    tmpPar.Value = param.Value;
                    comm.Parameters.Add(tmpPar);
                }
            }
        }

        public static IDataReader ExecuteReader(SelectHelper query, IDbConnection cnx)
        {
            return DbHelper.ExecuteReader(query.BuildCommandText(), query.Parametros, cnx, CommandType.Text);
        }

        public static IDataReader ExecuteReader(string commandText, IDictionary<string, object> pars, IDbConnection cnx, CommandType cmdType = CommandType.StoredProcedure)
        {
            IDataReader response;
            using (var cmd = cnx.CreateCommand())
            {
                cmd.CommandText = commandText;
                cmd.CommandType = cmdType;

                AddParameters(cmd, pars);

                response = cmd.ExecuteReader();
            }

            return response;
        }

        public static void AddParameter<TPar>(Dictionary<string, object> pars, string name, TPar value)
        {

            if (EqualityComparer<TPar>.Default.Equals(value, default(TPar)))
            {
                pars.Add(name, DBNull.Value);
            }
            else
            {
                pars.Add(name, value);
            }
        }

    }
}
