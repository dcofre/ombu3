﻿using System.Collections.Generic;
using System.Text;

namespace ombu3.Kernel.DAL.Helpers
{
    public class SelectHelper
    {

        public SelectHelper()
        {
            this.Parametros = new Dictionary<string, object>();
        }

        public string Select { get; set; }

        public string From { get; set; }

        public string Where { get; set; }

        public string OrderBy { get; set; }

        public string GroupBy { get; set; }

        public string Having { get; set; }

        public Dictionary<string, object> Parametros { get; set; }

        public string BuildCommandText()
        {
            var q = new StringBuilder( "SELECT " + this.Select);

            if (!string.IsNullOrEmpty(this.From))
            {
                q.Append(" FROM " + this.From);
            }

            if (!string.IsNullOrEmpty(this.Where))
            {
                q.Append(" WHERE " + this.Where);
            }

            if (!string.IsNullOrEmpty(this.GroupBy))
            {
                q.Append(" GROUP BY " + this.GroupBy);
            }

            if (!string.IsNullOrEmpty(this.Having))
            {
                q.Append(" HAVING " + this.Having);
            }

            if (!string.IsNullOrEmpty(this.OrderBy))
            {
                q.Append(" ORDER BY " + this.OrderBy);
            }

            return q.ToString();
        }
    }
}
