﻿namespace ombu3.Kernel.DAL
{
    using System;
    using System.Data;

    public interface IUOW : IDisposable
    {
        void BeginTran(IsolationLevel isolation = IsolationLevel.ReadCommitted);
        void CommitTran();
        void CommitAndReopenTran();
        void RollbackTran();
    }
}
