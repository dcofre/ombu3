﻿namespace ombu3.Kernel.DAL
{
    /// <summary>
    /// Representa un objeto persistible en un repositorio
    /// </summary>
    public interface IPersistable 
    {
    }
}
