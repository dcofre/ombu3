﻿namespace ombu3.Kernel.DAL
{
    using Microsoft.Extensions.Logging;
    using System;


    public class BaseDBService : ConfigurableService, IDBService
    {
        public BaseDBService(IDBServiceDeps deps) : this(deps.LoggerFactory, deps.UoW, deps.Config, deps.Mapper, deps.Repo)
        {
            if (deps == null) throw new ArgumentNullException("deps");
        }

        protected BaseDBService(ILoggerFactory loggerFact, IUOW uow, IConfigProvider config, IMapperService mapper, IRepository repo) : base(loggerFact, config)
        {
            if (uow == null) throw new ArgumentNullException("uow");
            this.UoW = uow;

            if (mapper == null) throw new ArgumentNullException("mapper");
            this.MapSvc = mapper;

            if (repo == null) throw new ArgumentNullException("repo");
            this.Repo = repo;
        }

    public IUOW UoW { get; set; }
    public IMapperService MapSvc { get; set; }
    public IRepository Repo { get; set; }
}
}
