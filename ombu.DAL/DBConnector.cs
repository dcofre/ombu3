﻿using System;
using System.Data;
using System.Data.Common;
using Microsoft.Data.SqlClient;

namespace ombu3.Kernel.DAL
{
    public class DBConnector : IDisposable, IDBConnector
    {
        private readonly string _connString;
        private DbConnection _cnx;

        public static IDBConnector Build(string connectionString)
        {
            return new DBConnector(connectionString);
        }

        public DBConnector(string connectionString)
        {
            this._connString = connectionString;
        }

        public DbConnection Connect()
        {
            if (this._cnx == null)
            {
                this._cnx = new SqlConnection( this._connString);
            }
            if (this._cnx.State != ConnectionState.Open) this._cnx.Open();
            return this._cnx;
        }

        public IDbConnection IConnection()
        {
            return (IDbConnection)this.Connect();
        }

        public void Close()
        {
            if (this._cnx != null)
            {
                this._cnx.Close();
                this._cnx.Dispose();
                this._cnx = null;
            }
        }

        #region IDisposable Support
        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    this.Close();
                }
                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
