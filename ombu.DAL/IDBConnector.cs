﻿using System;
using System.Data;
using System.Data.Common;

namespace ombu3.Kernel.DAL
{
    public interface IDBConnector : IDisposable
    {
        DbConnection Connect();
        IDbConnection IConnection();
        void Close();
    }
}