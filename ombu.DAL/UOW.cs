﻿namespace ombu3.Kernel.DAL
{
    using ombu3.Kernel.Base.Service;
    using Microsoft.Extensions.Logging;
    using System;
    using System.Collections.Generic;
    using System.Data;

    public class UOW : IUOW
    {
        private IBaseServiceDeps _deps;
        private ILogger _logger;

        /// <summary>
        /// Unit of work
        /// </summary>
        public UOW(IBaseServiceDeps deps, bool disablePersistance = false)
        {
            if (deps == null) throw new ArgumentNullException("IBaseServiceDeps");
            _deps = deps;
            _logger = deps.Logger;
        }


        public void BeginTran(IsolationLevel isolation = IsolationLevel.ReadCommitted)
        {
            try
            {
                //this.Session.BeginTransaction(isolation);
                //_logger.LogTrace(String.Format("{0} Ok. SID {1}", isolation, ""));
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, ex.Message, null);
                throw ex;
            }
        }

        public void CommitAndReopenTran()
        {
            throw new NotImplementedException();
        }

        public void CommitTran()
        {
            throw new NotImplementedException();
        }

        public void RollbackTran()
        {
            throw new NotImplementedException();
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~UOW() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion


    }
}