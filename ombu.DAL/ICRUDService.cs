﻿namespace ombu3.Kernel.DAL
{
    using ombu3.Kernel.Base.DTOs;
    using System.Collections.Generic;

    public interface ICRUDService<TDTO, TId> : IDBService
       where TDTO : IDTO 
    {
        TDTO Read(TId id);
        IList<TDTO> ReadAll();
        TDTO Insert(TDTO dto);
        TDTO Update(TDTO dto);
        void Delete(TId id);
    }
}