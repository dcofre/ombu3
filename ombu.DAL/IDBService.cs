﻿using ombu3.Kernel.Base.Service;

namespace ombu3.Kernel.DAL
{
    public interface IDBService : IBaseService
    {
        IUOW UoW { get; set; }
        IMapperService MapSvc { get; set; }
        IRepository Repo { get; set; }
    }
}