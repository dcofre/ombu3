﻿namespace ombu3.Kernel.DAL
{
    using AutoMapper;
    using ombu3.Kernel.Base.Service;

    public interface IDBServiceDeps : IConfigurableServiceDeps
    {
        IUOW UoW { get; set; }
        IMapperService Mapper { get; set; }
        IRepository Repo { get; set; }
    }
}