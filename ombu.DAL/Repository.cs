﻿namespace ombu3.Kernel.DAL
{
    using ombu3.Kernel.DAL.Entities;
    using Dapper;
    using DapperExtensions;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class Repository : IRepository
    {
        public Repository(IDBConnector db)
        {
            this.db = db;
        }

        protected IDBConnector db { get; private set; }

        public int Count<TEntity>(object filters) where TEntity : class
        {
            throw new NotImplementedException();
        }

        public IList<TEntity> GetAll<TEntity>() where TEntity : class, IEntity
        {
            return this.db.Connect().GetList<TEntity>().ToList();
        }

        public IList<TEntity> GetByFilter<TEntity>(string sql, object parameters) where TEntity : class, IEntity
        {
            return this.db.IConnection().Query<TEntity>(sql, parameters).ToList();
        }

        public TEntity GetOne<TEntity>(object id) where TEntity : class, IEntity
        {
            return this.db.Connect().Get<TEntity>(id);
        }

        public TEntity GetOne<TEntity>(string sql, object parameters, QueryOption option = QueryOption.SingleOrDefault) where TEntity : class, IEntity
        {
            TEntity retVal = null;
            switch (option)
            {
                case QueryOption.Single:
                    retVal = this.db.IConnection().QuerySingle<TEntity>(sql, parameters);
                    break;
                case QueryOption.First:
                    retVal = this.db.IConnection().QueryFirst<TEntity>(sql, parameters);
                    break;
                case QueryOption.FirstOrDefault:
                    retVal = this.db.IConnection().QueryFirstOrDefault<TEntity>(sql, parameters);
                    break;
                default:
                    retVal = this.db.IConnection().QuerySingleOrDefault<TEntity>(sql, parameters);
                    break;
            }
            return retVal;
        }

        public TEntity Insert<TEntity>(TEntity entity) where TEntity : class, IEntity
        {
            var bent = entity as BaseEntity;
            if (bent != null) bent.DoPreSave();
            
            this.db.Connect().Insert<TEntity>(entity);
            return entity;
        }

        public TEntity Update<TEntity>(TEntity entity) where TEntity : class, IEntity
        {
            var bent = entity as BaseEntity;
            if (bent != null) bent.DoPreSave();

            var upd = this.db.Connect().Update<TEntity>(entity);
            if (upd)
            {
                return entity;
            }
            else
            {
                return null;
            }
        }

        public TEntity Upsert<TEntity>(TEntity entity) where TEntity : class, IEntity
        {
            var bent = entity as BaseEntity;
            if (bent != null) bent.DoPreSave();

            var upd = this.db.Connect().Update<TEntity>(entity);
            if (upd)
            {
                return entity;
            }
            else
            {
                return this.Insert<TEntity>(entity);
            }
        }

        public bool Delete<TEntity>(object id) where TEntity : class, IEntity
        {
            TEntity ent = this.GetOne<TEntity>(id);
            if (ent != null)
            {
                return this.db.Connect().Delete<TEntity>(ent);
            }
            else
            {
                return false;
            }
        }
    }
}
