﻿namespace ombu3.Kernel.DAL
{
    using ombu3.Kernel.Base.Service;

    public class ConfigurableServiceDeps : BaseServiceDeps, IConfigurableServiceDeps
    {
        public ConfigurableServiceDeps(IBaseServiceDeps deps, IConfigProvider config) : base(deps.LoggerFactory)
        {
            this.Config = config;
        }

        public IConfigProvider Config { get; set; }
    }
}
