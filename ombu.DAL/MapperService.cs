﻿namespace ombu3.Kernel.DAL
{
    using AutoMapper;
    using ombu3.Kernel.Base.DTOs;
    using ombu3.Kernel.DAL.Entities;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class MapperService : ConfigurableService, IMapperService
    {
        public MapperService(IConfigurableServiceDeps deps, IMapper mapper) : base(deps)
        {
            if (mapper == null) throw new ArgumentNullException("mapper");
            this.Mapper = mapper;
        }

        protected IMapper Mapper { get; set; }

        /// <summary>
        /// Mapea entity a dto
        /// </summary>
        /// <param name="ent"></param>
        /// <returns>IDTO</returns>
        public virtual TDTO ToDTO<TDTO, TEntity>(TEntity ent) where TDTO : IDTO where TEntity : IEntity
        {
            TDTO retVal = default(TDTO);
            try
            {
                if (ent != null && this.Mapper != null) retVal = this.Mapper.Map<TEntity, TDTO>(ent);
            }
            catch (Exception ex)
            {
                this.ManageException(ex);
            }
            return retVal;
        }

        /// <summary>
        /// Mapea una lista de entities a una lista de dtos
        /// </summary>
        /// <param name="entList"></param>
        /// <returns>IDTO</returns>
        public virtual IList<TDTO> ToDTO<TDTO, TEntity>(IList<TEntity> entList) where TDTO : IDTO where TEntity : IEntity
        {
            var retVal = new List<TDTO>();
            try
            {
                if (entList != null)
                {
                    retVal = entList.Select(e => this.Mapper.Map<TEntity, TDTO>(e)).ToList();
                }
            }
            catch (Exception ex)
            {
                this.ManageException(ex);
            }
            return retVal;
        }


        /// <summary>
        /// Mapea entity a dto
        /// </summary>
        /// <param name="dto"></param>
        /// <returns>IDTO</returns>
        public virtual TEntity ToEntity<TDTO, TEntity>(TDTO dto) where TDTO : IDTO where TEntity : IEntity
        {
            TEntity retVal = default(TEntity);
            try
            {
                if (dto != null && this.Mapper != null) retVal = this.Mapper.Map<TDTO, TEntity>(dto);
            }
            catch (Exception ex)
            {
                this.ManageException(ex);
            }
            return retVal;
        }


        /// <summary>
        /// Mapea una lista de dtos a una lista de entities
        /// </summary>
        /// <param name="entList"></param>
        /// <returns>IDTO</returns>
        public virtual IList<TEntity> ToEntity<TDTO, TEntity>(IList<TDTO> entList) where TDTO : IDTO where TEntity : IEntity
        {
            var retVal = new List<TEntity>();
            try
            {
                if (entList != null)
                {
                    retVal = entList.Select(e => this.Mapper.Map<TDTO, TEntity>(e)).ToList();
                }
            }
            catch (Exception ex)
            {
                this.ManageException(ex);
            }
            return retVal;
        }
    }

}
