﻿using System;

namespace ombu3.Kernel.DAL.Entities
{
    public abstract class BaseGenericEntity<TId> : BaseEntity, IEntity<TId>
    {
        public static Type IdType { get { return typeof(TId); } }

        public virtual TId id { get; set; }


        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            if (obj == this) return true;

            IEntity<TId> that = default(IEntity<TId>);
            try
            {
                that = (IEntity<TId>)obj;
            }
            catch { }

            if (that == null)
            {
                return false;
            }
            else
            {
                return (this.id.Equals(that.id));
            }
        }

        public override int GetHashCode()
        {
            return id.GetHashCode();
        }

    }
}
