﻿namespace ombu3.Kernel.DAL.Entities
{
    using DapperExtensions.Mapper;

    public class BaseEntityMap<T> : ClassMapper<T> where T : BaseEntity
    {
        public BaseEntityMap()
        {
            Map(x => x.byuser);
            Map(x => x.created_at);
            Map(x => x.deleted_at);
            Map(x => x.updated_at);
        }
    }
}
