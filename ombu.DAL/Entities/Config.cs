﻿namespace ombu3.Kernel.DAL.Entities
{
    public class Config : BaseGenericEntity<int>
    {
        public Config() { }
        public virtual string ModuleId { get; set; }
        public virtual string Key { get; set; }
        public virtual string Environment { get; set; }
        public virtual string Value { get; set; }
        public virtual string Description { get; set; }
    }
}

