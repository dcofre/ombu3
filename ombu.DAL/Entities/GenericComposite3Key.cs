﻿namespace ombu3.Kernel.DAL.Entities
{
    using System;

    public class GenericComposite3Key<TKey1, TKey2, TKey3>
    {
        public virtual TKey1 id1 { get; set; }
        public virtual TKey2 id2 { get; set; }
        public virtual TKey3 id3 { get; set; }

        public GenericComposite3Key()
        { }

        public GenericComposite3Key(TKey1 id1, TKey2 id2, TKey3 id3)
        {
            this.id1 = id1;
            this.id2 = id2;
            this.id3 = id3;
        }

        public override bool Equals(object obj)
        {
            if (obj == null) return false;

            if (obj == this) return true;


            var that = obj as GenericComposite3Key<TKey1, TKey2, TKey3>;
            if (that == null)
            {
                return false;
            }
            else
            {
                return (this.id1.Equals(that.id1) && this.id2.Equals(that.id2) && this.id3.Equals(that.id3));
            }
        }

        public override int GetHashCode()
        {
            return id1.GetHashCode() ^ id2.GetHashCode() ^ id3.GetHashCode();
        }
    }
}
