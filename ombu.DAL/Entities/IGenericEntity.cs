﻿using ombu3.Kernel.Base.Validation;

namespace ombu3.Kernel.DAL.Entities
{
    /// <summary>
    /// Representa una entidad persistible y trackeable
    /// </summary>
    public interface IEntity<TId> : IEntity, IValidatable         
    {
        TId id { get; set; }
    }
}
