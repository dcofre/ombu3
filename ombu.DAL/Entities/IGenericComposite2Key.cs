﻿namespace ombu3.Kernel.DAL.Entities
{
    public interface IGenericComposite2Key<TKey1, TKey2>
    {
        TKey1 id1 { get; set; }
        TKey2 id2 { get; set; }
    }
}