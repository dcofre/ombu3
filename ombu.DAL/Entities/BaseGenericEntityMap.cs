﻿using DapperExtensions.Mapper;

namespace ombu3.Kernel.DAL.Entities
{
    public class BaseGenericEntityMap : ClassMapper<IEntity>
    {
        protected void MapEntity()
        {
            Map(e => e.created_at);
            Map(e => e.deleted_at);
            Map(e => e.updated_at);
            Map(e => e.byuser);
        }
    }
}
