﻿using ombu3.Kernel.Base.Base.Exceptions;
using ombu3.Kernel.Base.Base.Validation;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ombu3.Kernel.DAL.Entities
{
    public abstract class BaseEntity : IEntity
    {
        private IList<ValidationError> _validationErrors;

        protected BaseEntity()
        {
            _validationErrors = new List<ValidationError>();
        }

        public virtual IList<ValidationError> ValidationErrors
        {
            get
            {
                return _validationErrors;
            }
        }

        //dejar virtual para mockeo
        public virtual bool IsValid
        {
            get
            {
                return _validationErrors.Count == 0;
            }
        }

        public virtual void AddError(string errorMessage)
        {
            this.ValidationErrors.Add(new ValidationError(errorMessage));
        }

        public virtual DateTime? created_at { get; set; }
        public virtual DateTime? updated_at { get; set; }
        public virtual DateTime? deleted_at { get; set; }
        public virtual string byuser { get; set; }


        public virtual void OnPostLoad()
        { }

        public virtual void OnPreSave()
        {  }

        public virtual void Validate()
        { }

        public virtual void DoPostLoad()
        {
            this.OnPostLoad();
        }

        public virtual void DoPreSave()
        {
            this.StampTrackFields();
            this.OnPreSave();
            this.DoValidation();
        }

        private void DoValidation()
        {
            this.ValidationErrors.Clear();
            this.Validate();
            if (this.ValidationErrors.Any())
            {
                throw new ValidationException(this.ValidationErrors);
            }
        }

        protected virtual void StampTrackFields()
        {
            if (this.created_at == null)
            {
                this.created_at = DateTime.Now;
            }
            this.updated_at = DateTime.Now;
            this.byuser = string.Empty;
        }

        public virtual void AddValidationError(string errorMessage)
        {
            this.ValidationErrors.Add(new ValidationError(errorMessage));
        }
    }
}
