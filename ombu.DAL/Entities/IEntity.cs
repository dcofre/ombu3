﻿namespace ombu3.Kernel.DAL.Entities
{
    using ombu3.Kernel.Base.Validation;
    using System;

    /// <summary>
    /// Representa una entidad persistible y trackeable
    /// </summary>
    public interface IEntity: IValidatable
    {        
        DateTime? created_at { get; set; }
        DateTime? updated_at { get; set; }
        DateTime? deleted_at { get; set; }
        string byuser { get; set; }
        void OnPostLoad();
        void OnPreSave();
    }
}
