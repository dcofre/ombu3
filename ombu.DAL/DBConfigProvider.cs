﻿namespace ombu3.Kernel.DAL
{
    using ombu3.Kernel.Base.Base.Exceptions;
    using ombu3.Kernel.Base.Service;
    using ombu3.Kernel.DAL.Entities;
    using Microsoft.Extensions.Logging;
    using System;
    using System.Collections.Generic;

    public class DBConfigProvider : BaseService, IConfigProvider
    {
        public DBConfigProvider(ILoggerFactory logger, IUOW uow) : base(new BaseServiceDeps(logger))
        {
            if (uow == null) throw new ArgumentNullException("uow");
            this.UoW = uow;
            //this.Environment = ConfigurationManager.AppSettings.Get("Environment") ?? string.Empty;
            this.Cache = new Dictionary<string, Config>();
        }

        protected virtual string Environment { get; private set; }
        public IUOW UoW { get; set; }

        private IDictionary<string, Config> Cache { get; set; }

        public virtual string ModuleId
        {
            get
            {
                return "BASE";
            }
        }

        public Config Get(string key)
        {
            return GetFromDB(key, this.ModuleId);
        }

        public Config Get(string key, string moduleId)
        {
            Config retVal = null;
            var compKey = this.GetComposedKey(key, moduleId);
            if (!this.Cache.TryGetValue(compKey, out retVal))
            {
                retVal = GetFromDB(key, moduleId);
                if (retVal != null)
                {
                    this.Cache.Add(compKey, retVal);
                    this.Logger.LogDebug(string.Format("[{0}] added to cache", compKey));
                }
            }
            return retVal;
        }

        private Config GetFromDB(string key, string moduleId)
        {
            Config retVal = null;
            try
            {
                //var entity = this.UoW.RepoCache<Entities.Config>().Find(c => c.ModuleId == moduleId && c.Key == key && c.Environment == this.Environment).SingleOrDefault();
                //var logMsg = string.Format("Config {0}: Get ModuleId '{1}' Key '{2}'", this.Environment, moduleId, key);

                //if (entity != null)
                //{
                //    retVal = (ConfigDTO)entity.Map();
                //    Logger.LogDebug(logMsg + " OK");
                //}
                //else
                //{
                //    Logger.LogWarning(logMsg + " NOT FOUND!");
                //}
            }
            catch (Exception ex)
            {
                Logger.LogCritical(ex, ex.Message, null);
                throw new ServiceException(string.Format("Couldn't get key {0} module {1} from config repository", key, moduleId), ex);
            }

            return retVal;
        }

        public Config Upsert(Config dto)
        {
            Config retVal = null;
            try
            {

            }
            catch (Exception ex)
            {
                Logger.LogCritical(ex, ex.Message, null);
                throw new ServiceException("Couldn't upsert entity", ex);
            }

            return retVal;
        }


        public IDictionary<string, Config> GetAll()
        {
            return this.GetAll(this.ModuleId);
        }

        public IDictionary<string, Config> GetAll(string moduleId)
        {
            Dictionary<string, Config> retVal = null;
            try
            {
                //retVal = this.UoW.RepoCache<Entities.Config>().Find(c => c.ModuleId == moduleId && c.Environment == this.Environment)
                //        .Select(c => (ConfigDTO)c.Map())
                //        .ToDictionary(c => c.Key);
                Logger.LogDebug(string.Format("Config: GetAll for ModuleId {0}", moduleId));
            }
            catch (Exception ex)
            {
                Logger.LogCritical(ex, ex.Message, null);
                throw new ServiceException(string.Format("Couldn't get all configs from module '{0}'", moduleId), ex);
            }
            return retVal;
        }

        private string GetComposedKey(string key, string moduleId)
        {
            return moduleId.Trim().PadRight(10) + key.Trim();
        }

    }
}
