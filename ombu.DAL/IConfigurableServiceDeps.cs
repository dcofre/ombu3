﻿namespace ombu3.Kernel.DAL
{
    using ombu3.Kernel.Base.Service;

    public interface IConfigurableServiceDeps: IBaseServiceDeps
    {
        IConfigProvider Config { get; set; }
    }
}