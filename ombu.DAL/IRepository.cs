﻿using ombu3.Kernel.DAL.Entities;
using System.Collections.Generic;

namespace ombu3.Kernel.DAL
{
    public enum QueryOption
    {
        Single,
        SingleOrDefault,
        First,
        FirstOrDefault
    }
    /// <summary>
    /// Repositorio de objetos persistibles
    /// </summary>
    public interface IRepository
    {
        int Count<TEntity>(object filters) where TEntity : class;
        IList<TEntity> GetAll<TEntity>() where TEntity : class, IEntity;
        TEntity GetOne<TEntity>(object id) where TEntity : class, IEntity;
        TEntity GetOne<TEntity>(string sql, object parameters, QueryOption option = QueryOption.SingleOrDefault) where TEntity : class, IEntity;
        IList<TEntity> GetByFilter<TEntity>(string sql, object parameters) where TEntity : class, IEntity;
        TEntity Insert<TEntity>(TEntity entity) where TEntity : class, IEntity;
        TEntity Update<TEntity>(TEntity entity) where TEntity : class, IEntity;
        TEntity Upsert<TEntity>(TEntity entity) where TEntity : class, IEntity;
        bool Delete<TEntity>(object id) where TEntity : class, IEntity;
    }
}
